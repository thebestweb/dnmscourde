"use strict";
const { appId, appSecret } = require("wx-common");
exports.main = async (event, context) => {
  //event为客户端上传的参数
  console.log("event : ", event);

  const db = uniCloud.database();

  const { code } = event;

  const res = await uniCloud.httpclient.request(
    `https://api.weixin.qq.com/sns/jscode2session?appid=${appId}&secret=${appSecret}&js_code=${code}&grant_type=authorization_code`,
    { dataType: "json" }
  );

  const openid = res.data.openid;

  console.log(res);

  let userData = {
    openid,
    nickName: "微信用户",
    avatarUrl: "",
  };

  await db.collection("users").add(userData);

  //返回数据给客户端
  return userData;
};
